﻿using ELI_FREEMIUM_EXPORT.GaService;
using System.Collections.Generic;

namespace ELI_FREEMIUM_EXPORT.Model
{
    public class UserAgglomerate
    {    
        public Subscriber Subscriber { get; set; }
        public List<User> Users { get; set; }
    }
}
