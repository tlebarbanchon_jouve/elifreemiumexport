﻿using System;

namespace ELI_FREEMIUM_EXPORT.Model
{
    public class GaUser
    {
        public long SubscriberId { get; set; }
        public string Email { get; set; }
        public string Civilite { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public DateTime SubscriptionDate { get; set; }
        public string RaisonSociale { get; set; }
        public bool Professionnel { get; set; }
        public bool Optin { get; set; }
    }
}
