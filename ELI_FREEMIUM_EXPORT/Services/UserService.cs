﻿using ELI_FREEMIUM_EXPORT.GaService;
using ELI_FREEMIUM_EXPORT.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace ELI_FREEMIUM_EXPORT.Services
{
    public class UserService
    {      
        public static List<UserAgglomerate> GetGaUsersFromSubscribers()
        {
            List<UserAgglomerate> users = new List<UserAgglomerate>();

            using (GaPortClient client = new GaPortClient("WebServicesGA"))
            {
                Console.WriteLine($"Debut de l'export");
                int page = 1;
                int maxPerPage = 10;
                List<Subscriber> subscribers = new List<Subscriber>();

                var requete = new FindSubscribersByCriteriaRequest
                {
                    criteria = new CriteriaSubscriber
                    {
                        productCode = "ELIFREEMIUM"                        
                    },
                    maxPerPage = maxPerPage,
                    maxPerPageSpecified = true,
                    startPage = page,
                    startPageSpecified = true,
                    linkedConnections = true,
                    
                };


                var response = client.findSubscribersByCriteria(requete);
                
                if (response.operationResult.status == Status.OK && response.subscribers != null && response.subscribers.Any())
                {
                    subscribers.AddRange(response.subscribers);

                    while (response.numResults > subscribers.Count)
                    {
                        requete.startPage++;
                        response = client.findSubscribersByCriteria(requete);
                        if (response.operationResult.status == Status.OK && response.subscribers != null && response.subscribers.Any())
                            subscribers.AddRange(response.subscribers);
                        else
                            break;
                    }

                    Console.WriteLine($"Récupération des subscribers OK {subscribers.Count()}");
                }

                if(subscribers != null && subscribers.Any())
                {
                    subscribers.ForEach(s => 
                    {
                        UserAgglomerate uc = new UserAgglomerate
                        {
                            Subscriber = s
                        };

                        var userResponse = client.findUserByCriteria(new FindUsersByCriteriaRequest
                        {
                            criteria = new CriteriaUser
                            {
                                login = s.email
                            },
                            maxPerPage = maxPerPage,
                            maxPerPageSpecified = true,
                            startPage = 1,
                            startPageSpecified = true                            
                        });

                        if(userResponse.numResults > maxPerPage)
                        {
                            throw new Exception("Hahahahahaha ! Tu dois coder une pagination !");
                        }

                        if (userResponse.operationResult.status == Status.OK && userResponse.users != null && userResponse.users.Any())
                        {
                            uc.Users = new List<User>(userResponse.users);
                        }

                        users.Add(uc);                        
                    });
                }
            }
            return users;
        }
        
        public static List<GaUser> TransformData(List<UserAgglomerate> users)
        {
            List<GaUser> gaUsers = new List<GaUser>();

            if(users != null && users.Any())
            {
                users.Where(u => u != null).ToList().ForEach(u => 
                {
                    //Recupération de la date de création du produit ELIFREEMIUM
                    var date = u.Subscriber.connections.SelectMany(c => c.subscriptions).Single(s => s.product != null && s.product.code == "ELIFREEMIUM").creationDate;

                    User user = null;

                    if(u.Users != null)
                        user = u.Users.FirstOrDefault();
                    
                    var gaUser = new GaUser
                    {
                        SubscriptionDate = date,
                        Civilite = user != null ? user.civility : u.Subscriber.civility,
                        Email = user != null ? user.login : u.Subscriber.email,
                        Nom = user != null ? user.name : u.Subscriber.name,
                        Prenom = user != null ? user.firstname : u.Subscriber.firstname,
                        RaisonSociale = user != null ? user.companyName : u.Subscriber.companyName,
                        SubscriberId = u.Subscriber.id,
                        Optin = user != null && user.optIns != null && user.optIns.Any(o => o.option.code == "GA_OPT1") ? true : false,
                        Professionnel = user != null ? user.professionnel : false

                    };
                    gaUsers.Add(gaUser);
                });
            }

            return gaUsers;
        }

        public static void CorrectProfessionnalsUsers(List<UserAgglomerate> users)
        {            
            //Récupération de la conf pour la localisation du fichier de log
            string correctionFileLocation = ConfigurationManager.AppSettings["professionnalCorrectionFileLocation"];

            using (LogService log = new LogService(correctionFileLocation))
            {
                bool debugMode = true;

                if(!bool.TryParse(ConfigurationManager.AppSettings["professionnalCorrectionDebug"], out debugMode))
                {
                    throw new ArgumentException($"La clé professionnalCorrectionDebug est absente de l'app config");
                }


                log.WriteLine("Lancement du rattrapage des comptes pro");
                log.WriteLine($"Mode debug : {debugMode}");

                if (!debugMode)
                {
                    Console.WriteLine("Attention, les données vont être rattrapées ! Continuer ? O ou N");
                    var key = Console.ReadKey();
                    if (key.Key != ConsoleKey.O)
                    {
                        log.WriteLine($"Fin du processus à la demande de l'utilisateur");
                        return;
                    }
                }
            
                // On travaille sur les Users
                var flattenUsersToCorrect = users.Where(u => u.Users != null && u.Users.Any()).SelectMany(u => u.Users).Where(f => !f.professionnel && !string.IsNullOrEmpty(f.companyName)).ToList();

                if (flattenUsersToCorrect.Any())
                {
                    log.WriteLine($"Nombre de comptes à rattraper : {flattenUsersToCorrect.Count()}");
                    
                    flattenUsersToCorrect.ToList().ForEach(f => 
                    {
                        log.WriteLine($"Traitement user : {f.id} - {f.firstname} - {f.name} - {f.companyName}");

                        if (!debugMode)
                        {
                            f.professionnel = true;
                            f.professionnelSpecified = true;

                            using (GaPortClient client = new GaPortClient("WebServicesGA"))
                            {
                                var requete = new ModifyUserRequest
                                {
                                    user = f
                                };

                                log.WriteLine($"Traitement user : Appel WS mise à jour");
                                var response = client.modifyUser(requete);

                                if (response.operationResult.status == Status.OK)
                                {
                                    log.WriteLine($"Traitement user : Appel WS mise à jour OK");
                                }
                                else
                                {
                                    log.WriteLine($"Traitement user : Appel WS mise à jour KO");
                                }
                            }                        
                        }
                    });
                }

                log.WriteLine("Fin du rattrapage des comptes pro");
            }
        }
    }
}
