﻿using ELI_FREEMIUM_EXPORT.Model;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace ELI_FREEMIUM_EXPORT.Services
{
    public static class ExcelService
    {
        public static void GenerateExcelFile(List<GaUser> user)
        {
            //Récupération de la conf pour la localisation du fichier
            string exportFileLocation = ConfigurationManager.AppSettings["exportFileLocation"];


            using (ExcelPackage excel = new ExcelPackage())
            {
                int row = 2;
                excel.Workbook.Worksheets.Add("Export");

                var headerRow = new List<string[]>()
                {
                    new string[] { "Subscriber Id", "Civilite", "Prenom", "Nom", "Email", "Date d'inscription", "Est professionnel", "Raison sociale", "Accepte les sollicitations" }
                };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets["Export"];

                // Popular header row data
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                if (user != null && user.Any())
                {
                    user.ForEach(u =>
                    {
                        worksheet.SetValue(row, 1, u.SubscriberId);
                        worksheet.SetValue(row, 2, u.Civilite);
                        worksheet.SetValue(row, 3, u.Prenom);
                        worksheet.SetValue(row, 4, u.Nom);
                        worksheet.SetValue(row, 5, u.Email);
                        worksheet.SetValue(row, 6, u.SubscriptionDate.ToShortDateString());
                        worksheet.SetValue(row, 7, u.Professionnel);
                        worksheet.SetValue(row, 8, u.RaisonSociale);
                        worksheet.SetValue(row, 9, u.Professionnel || u.Optin);
                        row++;
                    });
                }
                
                FileInfo excelFile = new FileInfo(exportFileLocation);
                excel.SaveAs(excelFile);
            }
        }
    }
}
