﻿using ELI_FREEMIUM_EXPORT.Model;
using ELI_FREEMIUM_EXPORT.Services;
using System.Collections.Generic;
using System.Linq;

namespace ELI_FREEMIUM_EXPORT
{
    class Program
    {
        static void Main(string[] args)
        {
            List<UserAgglomerate> user = UserService.GetGaUsersFromSubscribers();
            
            UserService.CorrectProfessionnalsUsers(user);

            List<GaUser> gaUsers = UserService.TransformData(user);
            
            ExcelService.GenerateExcelFile(gaUsers);
        }

        

        
    }
}
